SmieszneMemy::Application.routes.draw do
  resources :mems do
  	collection do
  		get 'my'
  		get 'inactive'
  	end
  end

  root 'mems#index'
  get "home/index"
  devise_for :users
  devise_for :admins
  mount RailsAdmin::Engine => '/admin', :as => 'rails_admin'
 
end
